const abstractMethodErrorMessage = 'Method is not implemented';

class BaseDriver {
    constructor(host, port, dbName) {
        this._host = host;
        this._port = port;
        this._dbName = dbName;
        this._connected = false;
        this._db = null;
    }

    /**
     * True if driver is connected
     *
     * @returns {Boolean}
     */
    get connected() {
        return this._connected;
    }

    /**
     * Raw DB connection
     *
     * @returns {Object|null}
     */
    get connection() {
        return this._db;
    }

    /**
     * Host name
     *
     * @returns {String}
     */
    get host() {
        return this._host;
    }

    /**
     * Database name
     *
     * @returns {String}
     */
    get dbName() {
        return this._dbName;
    }
    /**
     * Checks authorization
     *
     * @abstract
     * @param {String} token
     * @returns {Object|Promise|null}
     */
    authToken(token) { // eslint-disable-line no-unused-vars
        throw new Error(abstractMethodErrorMessage);
    }

    /**
     * Connect to the database
     *
     * @abstract
     */
    connect() {
        throw new Error(abstractMethodErrorMessage);
    }

    /**
     * Disconnection from the database server
     *
     * @abstract
     * @param {Boolean} force
     * @return {Promise|null}
     */
    disconnect(force = true) { // eslint-disable-line no-unused-vars
        throw new Error(abstractMethodErrorMessage);
    }
}

module.exports = BaseDriver;
