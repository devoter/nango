const MongoClient = require('mongodb').MongoClient;
const BaseDriver = require('./base-driver');

class MongodbDriver extends BaseDriver {
    async connect() {
        this._dbConnection = await MongoClient.connect(`mongodb://${this._host}:${this._port}`);
        this._db = this._dbConnection.db(this._dbName);
        this._connected = true;
    }

    async disconnect(force = true) {
        await this._dbConnection.close(force);
    }

    authToken(token) {
        return this._db.collection('users').findOne({token: token.replace('Token ', '')});
    }
}

module.exports = MongodbDriver;
