const URL = require('url');
const http = require('http');
const https = require('https');
const NamedRegExp = require('named-regexp-groups');
const querystring = require('querystring');
const {isNone, arrayJoin} = require('./utils');
const DummyViewSet = require('./views/dummy-view-set');

class Nango {
    /**
     *
     * @param {Object} settings
     * @param {Array} urls
     * @param {Object} extra
     * @param {Object} logger
     */
    constructor(settings = {}, urls = [], extra = null, logger = console) {
        this._settings = settings;
        this._urls = urls;
        this._server = null;
        this._middleware = null;
        this._logger = logger;
        this._extra = extra;

        this._serverCore = this._serverCore.bind(this);
        this._initSettings();
        this._initUrls();
        this._initServer();
        this._initMiddleware();
        this._context = {
            settings: settings,
            logger: this._logger,
            extra: this._extra,
            db: settings.database.connection
        };
    }

    get extra() {
        return this._extra;
    }

    set extra(value) {
        this._extra = value;
        this._context.extra = value;
    }

    listen() {
        const settings = this._settings;
        this._server.listen(settings.port, settings.host, () =>
            this._logger.log(`Server running at ${settings.protocol}://${settings.host}:${settings.port}`));
    }

    _initMiddleware() {
        const middleware = new Array(this._settings.middleware.length);
        for (let i = 0; i < this._settings.middleware.length; ++i)
            middleware[i] = new this._settings.middleware[i];
        this._middleware = middleware;
    }

    _initSettings() {
        this._settings.middleware = this._settings.middleware || [];
        this._settings.database = this._settings.database || {};
        this._settings.protocol = this._settings.protocol || 'http';
        this._settings.host = this._settings.host || 'localhost';
        this._settings.port = this._settings.port || 8000;
        this._settings.allowedHosts = this._settings.allowedHosts || ['*'];
        this._settings.headers = this._settings.headers || {};
    }

    _initServer() {
        if (this._settings.protocol === 'http')
            this._server = http.createServer(this._serverCore);
        else
            this._server = https.createServer(this._serverCore);
    }

    _initUrls() {
        const urls = this._urls;
        const preparedUrls = [];
        for (let i = 0; i < urls.length; ++i) {
            const url = urls[i];

            if (url.length !== 4) { // create fake views set for function-based views
                const viewSet = new DummyViewSet(url[1]);
                viewSet.methods = url[2].slice();
                if (!viewSet.methods.includes('options')) {
                    viewSet.methods.push('options');
                    preparedUrls.push([new NamedRegExp(url[0]), {options: viewSet.options.bind(viewSet)}, viewSet]);
                }
                url[1] = viewSet.list.bind(viewSet);
                url.push(viewSet);
            }

            const preparedMethods = {};
            const methods = url[2];

            for (let j = 0; j < methods.length; ++j)
                preparedMethods[methods[j]] = url[1];

            preparedUrls.push([new NamedRegExp(url[0]), preparedMethods, url[3]]);
        }
        this._urls = preparedUrls;
    }

    _serverCore(req, res) {
        const body = [];
        req.on('data', chunk => body.push(chunk));

        const urls = this._urls;
        const methodName = req.method.toLowerCase();
        const reqUrl = URL.parse(req.url);
        const queryParams = querystring.parse(reqUrl.query);
        let pathName = reqUrl.pathname;
        if (!reqUrl.pathname.length || reqUrl.pathname.charAt(reqUrl.pathname.length - 1) !== '/')
            pathName += '/';
        let url = null, pathParams = null, handler = null;

        for (let i = 0; i < urls.length; ++i) {
            if (urls[i][0].test(pathName)) {
                pathParams = urls[i][0].exec(pathName).groups;
                url = urls[i];
                handler = url[1][methodName];
                if (handler)
                    break;
            }
        }

        if (url === null) {
            res.statusCode = 404; // url not found
            res.end();
            return;
        }

        if (!handler) {
            this._except(res, new Error('Route is found, but there is no handler'));
            return;
        }

        let allowedMethods = [];
        for (let methodName in url[1]) {
            if (!url[1].hasOwnProperty(methodName)) continue;
            allowedMethods.push(methodName);
        }
        allowedMethods = arrayJoin(allowedMethods, true);

        req.on('end', async () => {
            const middleware = this._middleware;
            const request = {
                orig: req,
                method: req.method,
                path: pathName,
                url: reqUrl,
                pathParams: pathParams || {},
                queryParams: queryParams || {},
                body: body,
                data: Nango.parseBody(body),
                allowedMethods: allowedMethods,
                viewSet: url[2]
            };

            const context = this._context;

            for (let i = 0; i < middleware.length; ++i) {
                let response = null;
                const mid = middleware[i];
                try {
                    response = await mid.processRequest(request, context);
                }
                catch (error) {
                    this._except(res, error);
                    return;
                }

                if(!isNone(response)) {
                    await this._responseWithMiddleware(res, request, response, middleware, context);
                    return;
                }
            }

            for (let i = 0; i < middleware.length; ++i) {
                let response = null;
                const mid = middleware[i];
                try {
                    response = await mid.processView(request, handler, context);
                }
                catch (error) {
                    this._except(res, error);
                    return;
                }

                if(!isNone(response)) {
                    await this._responseWithMiddleware(res, request, response, middleware, context);
                    return;
                }
            }

            let viewResponse = null;
            try {
                viewResponse = await handler(request, context);
            }
            catch (error) {
                await this._exceptWithMiddleware(res, request, error, middleware, context);
                return;
            }



            if (isNone(viewResponse)) {
                this._except(res, new Error('View must return a response object'));
                return;
            }

            if (viewResponse.templateName) {
                for (let i = middleware.length - 1; i >= 0; --i) {
                    let response = null;
                    const mid = middleware[i];
                    try {
                        response = await mid.processTemplateResponse(request, viewResponse, context);
                    }
                    catch (error) {
                        this._except(res, error);
                        return;
                    }

                    if (!isNone(response)) {
                        await this._responseWithMiddleware(res, request, response, middleware, context);
                        return;
                    }
                }
            }

            await this._responseWithMiddleware(res, request, viewResponse, middleware, context);
        });
    }

    _except(res, error) {
        this._logger.error(error);
        res.statusCode = 500;
        res.end();
    }

    async _exceptWithMiddleware(res, request, exception, middleware, context) {
        for (let i = middleware.length - 1; i >= 0; --i) {
            let response = null;
            const mid = middleware[i];
            try {
                response = await mid.processException(request, exception, context);
            }
            catch (error) {
                this._except(res, error);
                return;
            }

            if(!isNone(response)) {
                await this._responseWithMiddleware(res, request, response, middleware, context);
                return;
            }
        }
    }

    async _responseWithMiddleware(res, request, lastResponse, middleware, context) {
        let response;
        for (let i = middleware.length - 1; i >= 0; --i) {
            response = null;
            const mid = middleware[i];
            try {
                response = await mid.processResponse(request, lastResponse, context);
            }
            catch (error) {
                this._except(res, error);
                return;
            }

            if(!isNone(response))
                lastResponse = response;
        }

        lastResponse.headers = Object.assign({}, {'Access-Control-Allow-Methods': request.allowedMethods},
            lastResponse.headers ? lastResponse.headers : {});

        Nango._response(res, lastResponse);
    }

    static parseBody(source) {
        let body;
        try {
            body = JSON.parse(Buffer.concat(source).toString());
        }
        catch (error) {
            body = null;
        }

        return body;
    }

    static _response(res, response) {
        res.statusCode = response.status !== null ? response.status : 200;
        for (let h in response.headers) {
            if (!response.headers.hasOwnProperty(h)) continue;
            res.setHeader(h, response.headers[h]);
        }
        res.end(response.data);
    }
}

module.exports = Nango;
