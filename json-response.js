/**
 * Converts data to json
 *
 * @param {String|Object|Array|null} data
 * @param {Number|null} statusCode
 * @param {Object|null} headers
 * @returns {{data: String|null, headers: Object, status: Number|null, templateName: null}}
 */
function response(data = null, statusCode = null, headers = null) {
    return {
        data: typeof(data) === 'object' && data !== null ? JSON.stringify(data) : data,
        headers: Object.assign({}, headers ? headers : {}, {'Content-Type': 'application/json'}),
        status: statusCode,
        templateName: null
    };
}

module.exports = response;
