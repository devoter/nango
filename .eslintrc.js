module.exports = {
    root: true,
    parserOptions: {
        ecmaVersion: 2017,
        sourceType: 'script'
    },
    extends: 'eslint:recommended',
    env: {
        es6: true,
        node: true,
        browser: false,
        amd: true
    },
    rules: {
        semi: [
            "error",
            "always"
        ],
        "no-console": 0
    }
};
