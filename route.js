const checkMethods = require('./check-methods');

/**
 * Transforms base Url and views set to an array of urls
 * @param {String} baseUrl
 * @param {Function} viewSet
 * @param {Array} methods
 * @returns {Array}
 */
function route (baseUrl, viewSet, methods = null) {
    const urls = [];
    const viewInstance = new viewSet();
    const hasMethodToAction = typeof(viewInstance.methodToAction) === 'function';

    if (methods !== null)
        methods = checkMethods(methods);
    else if (Array.isArray(viewInstance.methods))
        methods = checkMethods(viewInstance.methods);
    else if (!hasMethodToAction) {
        methods = [];
        if (viewInstance.list || viewInstance.retrieve)
            methods.push('get');
        if (viewInstance.create)
            methods.push('post');
        if (viewInstance.update)
            methods.push('put');
        if (viewInstance.partialUpdate)
            methods.push('patch');
        if (viewInstance.remove)
            methods.push('delete');
    }
    else {
        let methodNames = ['get', 'post', 'put', 'patch', 'delete'];
        for (let i = 0; i < methodNames.length; ++i) {
            const name = methodNames[i];
            const handler = viewInstance.methodToAction(name);
            if (handler !== null)
                methods.push(name);
        }
    }

    methods.push('options');
    viewInstance.methods = methods;

    const manyUrl = baseUrl + '/$';
    const itemUrl = baseUrl + '/(?<pk>.+)/$';

    if (hasMethodToAction) {
        for (let i = 0; i < methods.length; ++i) {
            const method = methods[i];
            const handler = viewInstance.methodToAction(method);
            if (handler !== null)
                urls.push([handler.many ? manyUrl : itemUrl, handler.handler, [method], viewInstance]);
        }
    }
    else {
        for (let i = 0; i < methods.length; ++i) {
            const method = methods[i];
            if (method === 'get') {
                if (viewInstance.retrieve)
                    urls.push([itemUrl, viewInstance.retrieve.bind(viewInstance), ['get'], viewInstance]);
                if (viewInstance.list)
                    urls.push([manyUrl, viewInstance.list.bind(viewInstance), ['get'], viewInstance]);
            }
            else if (method === 'put' && viewInstance.update)
                urls.push([itemUrl, viewInstance.update.bind(viewInstance), ['put'], viewInstance]);
            else if (method === 'patch' && viewInstance.partialUpdate)
                urls.push([itemUrl, viewInstance.partialUpdate.bind(viewInstance), ['patch'], viewInstance]);
            else if (method === 'delete' && viewInstance.remove)
                urls.push([itemUrl, viewInstance.remove.bind(viewInstance), ['delete'], viewInstance]);
            else if (method === 'options' && viewInstance.options) {
                urls.push([itemUrl, viewInstance.options.bind(viewInstance), ['options'], viewInstance]);
                urls.push([manyUrl, viewInstance.options.bind(viewInstance), ['options'], viewInstance]);
            }
            else if (method === 'post' && viewInstance.create)
                urls.push([manyUrl, viewInstance.create.bind(viewInstance), ['post'], viewInstance]);
        }
    }

    return urls;
}

module.exports = route;
