function checkMethods(methods) {
    const validMethods = [];
    if (Array.isArray(methods)) {
        for (let i = 0; i < methods.length; ++i) {
            let method = null;
            switch(methods[i].toLowerCase()) {
                case 'get':
                    method = 'get';
                    break;
                case 'post':
                    method = 'post';
                    break;
                case 'put':
                    method = 'put';
                    break;
                case 'patch':
                    method = 'patch';
                    break;
                case 'delete':
                    method = 'delete';
                    break;
            }

            if (method !== null)
                validMethods.push(method);
        }
    }
    return validMethods;
}

module.exports = checkMethods;
