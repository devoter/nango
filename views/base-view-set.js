const response = require('../response');
const arrayJoin = require('../utils/array-join');

class BaseViewSet {
    constructor() {
        /**
         * Array of available request methods
         *
         * @type {Array|null}
         */
        this.methods = null;

        /**
         * Should return bound function of the method or null if not found
         *
         * @type {Function|null}
         * @param {String} method
         * @returns {{handler: Function, many: Boolean}|null}
         */
        this.methodToAction = this.methodToAction || null;

        /**
         * Retrieve request handler
         *
         * @type {Function|null}
         * @param {Object} request
         * @param {Object} context
         * @returns {{data: String|null, headers: Object, status: Number|null}|Promise} response
         */
        this.retrieve = this.retrieve || null;

        /**
         * List request handler
         *
         * @type {Function|null}
         * @param {Object} request
         * @param {Object} context
         * @returns {{data: String|null, headers: Object, status: Number|null}|Promise} response
         */
        this.list = this.list || null;

        /**
         * Create request handler
         *
         * @type {Function|null}
         * @param {Object} request
         * @param {Object} context
         * @returns {{data: String|null, headers: Object, status: Number|null}|Promise} response
         */
        this.create = this.create || null;

        /**
         * Update request handler
         *
         * @type {Function|null}
         * @param {Object} request
         * @param {Object} context
         * @returns {{data: String|null, headers: Object, status: Number|null}|Promise} response
         */
        this.update = this.update || null;

        /**
         * Partial update request handler
         *
         * @type {Function|null}
         * @param {Object} request
         * @param {Object} context
         * @returns {{data: String|null, headers: Object, status: Number|null}|Promise} response
         */
        this.partialUpdate = this.partialUpdate || null;

        /**
         * Remove request handler
         *
         * @type {Function|null}
         * @param {Object} request
         * @param {Object} context
         * @returns {{data: String|null, headers: Object, status: Number|null}|Promise} response
         */
        this.remove = this.remove || null;
    }

    /**
     * Options request handler
     *
     * @param {Object} request
     * @param {Object} context
     * @returns {{data: String|null, headers: Object, status: Number|null}|Promise} response
     */
    options(request, context) {
        const settings = context.settings;
        const headers = Object.assign({}, settings.headers);
        headers['Access-Control-Allow-Origin'] = arrayJoin(settings.allowedHosts);
        headers['Access-Control-Allow-Methods'] = arrayJoin(this.methods, true);
        headers['Access-Control-Allow-Credentials'] = false;
        headers['Access-Control-Max-Age'] = settings.cookieLifeTime;
        headers['Access-Control-Allow-Headers'] = 'X-Requested-With, X-HTTP-Method-Override, ' +
            'Content-Type, Accept, Authorization, Cache-Control';
        return response(null, 200, null, headers);
    }
}

module.exports = BaseViewSet;
