const BaseViewSet = require('./base-view-set');

class DummyViewSet extends BaseViewSet {
    constructor(view) {
        super();
        this.list = view;
    }
}

module.exports = DummyViewSet;
