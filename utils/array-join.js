/**
 * Joins an array with spaces
 *
 * @param {Array} arr
 * @param {Boolean} uppercase
 * @param {String} separator
 * @return {String}
 */
function arrayJoin(arr, uppercase = false, separator = ',') {
    let result = '';
    for (let i = 0; i < arr.length; ++ i) {
        if (i)
            result += separator + ' ';
        result += uppercase ? arr[i].toUpperCase() : arr[i];
    }
    return result;
}

module.exports = arrayJoin;
