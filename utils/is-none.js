function isNone(arg) {
    return arg === undefined || arg === null;
}

module.exports = isNone;
