class MongoLogger {
    constructor(config, connection = null) {
        this._db = connection;
        this._config = config;
    }

    info() {
        if (this._isLogLevelActual('info')) {
            const timestamp = new Date();
            if (this._config.db && this._db)
                this._db.collection('logs').insertOne({type: 'info', message: [...arguments].join(' '), timestamp: timestamp},
                    error => error ? console.error('Could not save log into database by the reason', error) : null);
            if (this._config.std)
                console.info('[info#' + timestamp + ']: ', ...arguments);
        }
    }

    log() {
        if (this._isLogLevelActual('log')) {
            const timestamp = new Date();
            if (this._config.db)
                this._db.collection('logs').insertOne({type: 'log', message: [...arguments].join(' '), timestamp: timestamp},
                    error => error ? console.error('Could not save log into database by the reason', error) : null);
            if (this._config.std)
                console.log('[log#' + timestamp + ']: ', ...arguments);
        }
    }

    warn() {
        if (this._isLogLevelActual('warn')) {
            const timestamp = new Date();
            if (this._config.db)
                this._db.collection('logs').insertOne({type: 'warn', message: [...arguments].join(' '), timestamp: timestamp},
                    error => error ? console.error('Could not save log into database by the reason', error) : null);
            if (this._config.std)
                console.warn('[warn#' + timestamp + ']: ', ...arguments);
        }
    }

    error() {
        if (this._isLogLevelActual('error')) {
            const timestamp = new Date();
            if (this._config.db)
                this._db.collection('logs').insertOne({type: 'error', message: [...arguments].join(' '), timestamp: timestamp},
                    error => error ? console.error('Could not save log into database by the reason', error) : null);
            if (this._config.std)
                console.error('[error#' + timestamp + ']: ', ...arguments);
        }
    }

    _isLogLevelActual(level) {
        const config = this._config;
        if (!config.enabled) return false;

        let configLevel = config.level;

        if (level === 'info' && configLevel === 'info')
            return true;
        else if (level === 'log' && (configLevel === 'info' || configLevel === 'log'))
            return true;
        else if (level === 'warn' && (configLevel === 'info' || configLevel === 'log' || configLevel === 'warn'))
            return true;
        else if (level === 'error' && (configLevel === 'info' || configLevel === 'log' ||
            configLevel === 'warn' || configLevel === 'error'))
            return true;
        return false;
    }
}

module.exports = MongoLogger;
