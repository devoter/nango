/**
 * Converts data to json
 *
 * @param {String|Object|Array|null} data
 * @param {Number|null} statusCode
 * @param {String|null} templateName
 * @param {Object|null} headers
 * @returns {{data: String|null, headers: Object, status: Number|null, templateName: String|null}}
 */
function response(data = null, statusCode = null, templateName = null, headers = null) {
    return {data: data, headers: headers, status: statusCode, templateName: templateName};
}

module.exports = response;
