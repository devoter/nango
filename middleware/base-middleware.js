/*eslint no-unused-vars: ["error", { "args": "none" }]*/

class BaseMiddleware {
    /**
     * Request hook
     *
     * @param {Object} request
     * @param {Object} context
     * @returns {{data: String|null, headers: Object, status: Number|null, templateName: String|null}|null}
     */
    processRequest(request, context) {
        return null;
    }

    /**
     * View hook
     *
     * @param {Object} request
     * @param {Function} view
     * @param {Object} context
     * @returns {{data: String|null, headers: Object, status: Number|null, templateName: String|null}|null}
     */
    processView(request, view, context) {
        return null;
    }

    /**
     * Template hook
     *
     * @param {Object} request
     * @param {Object} response
     * @param {Object} context
     * @returns {{data: String|null, headers: Object, status: Number|null, templateName: String|null}|null}
     */
    processTemplateResponse(request, response, context) {
        return null;
    }

    /**
     * Response hook
     *
     * @param {Object} request
     * @param {Object} response
     * @param {Object} context
     * @returns {{data: String|null, headers: Object, status: Number|null, templateName: String|null}|null}
     */
    processResponse(request, response, context) {
        return null;
    }

    /**
     * Exception hook
     *
     * @param {Object} request
     * @param {Object} exception
     * @param {Object} context
     * @returns {{data: String|null, headers: Object, status: Number|null, templateName: String|null}|null}
     */
    processException(request, exception, context) {
        return null;
    }
}

module.exports = BaseMiddleware;
