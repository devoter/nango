const BaseMiddleware = require('./base-middleware');
const response = require('../response');

class AuthRequiredMiddleware extends BaseMiddleware {
    processRequest(request, context) { // eslint-disable-line no-unused-vars
        return (request.method.toLowerCase() !== 'options' && request.viewSet.authRequired && !request.isAuthorized) ?
            response(null, 403) : null;
    }
}

module.exports = AuthRequiredMiddleware;
