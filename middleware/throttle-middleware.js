const BaseMiddleware = require('./base-middleware');
const response = require('../response');

class ThrottleMiddleware extends BaseMiddleware {
    constructor() {
        super();
        this._anonLastAccessTime = {};
        this._userLastAccessTime = {};
        this._lastAccessTime = [];
    }

    processRequest(request, context) {
        const accessTime =  Date.now();
        const settings = context.settings.throttle;
        if (!settings || request.method.toLowerCase() === 'options')
            return null;

        if (request.viewSet.throttle) {
            const viewSetInstance = request.viewSet;
            const period = ThrottleMiddleware._getPeriod(viewSetInstance.throttle);
            let found = -1;
            for (let i = 0; i < this._lastAccessTime.length; ++i) {
                if (this._lastAccessTime[i][0] === viewSetInstance) {
                    found = i;
                    break;
                }
            }

            if (found === -1) {
                if (!period)
                    return response(null, settings.responseCode || 403);
                else {
                    this._lastAccessTime.push(viewSetInstance, accessTime);
                    return null;
                }
            }
            else if (accessTime - this._lastAccessTime[found][1] < period)
                return response(null, settings.responseCode || 403);

            this._lastAccessTime[found][1] = accessTime;
            return null;
        }
        else if (!request.isAuthorized && settings.anonThrottle) {
            const period = ThrottleMiddleware._getPeriod(settings.anonThrottle);
            const client = request.orig.headers['x-forwarded-for'] ?
                request.orig.headers['x-forwarded-for'].split(',')[0] : request.orig.connection.remoteAddress;
            const lastAccessTime = this._anonLastAccessTime[client];
            if (!lastAccessTime) {
                if (!period)
                    return response(null, settings.responseCode || 403);
                else {
                    this._anonLastAccessTime[client] = accessTime;
                    return null;
                }
            }
            else if (accessTime - lastAccessTime < period)
                return response(null, settings.responseCode || 403);

            this._anonLastAccessTime[client] = accessTime;
            return null;
        }
        else if(request.isAuthorized && settings.userThrottle) {
            const period = ThrottleMiddleware._getPeriod(settings.userThrottle);
            const client = request.user.username;
            const lastAccessTime = this._userLastAccessTime[client];
            if (!lastAccessTime) {
                if (!period)
                    return response(null, settings.responseCode || 403);
                else {
                    this._userLastAccessTime[client] = accessTime;
                    return null;
                }
            }
            else if (accessTime - lastAccessTime < period)
                return response(null, settings.responseCode || 403);

            this._userLastAccessTime[client] = accessTime;
            return null;
        }

        return null;
    }

    /**
     *
     * @param {String} throttle
     * @returns {Number}
     * @private
     */
    static _getPeriod(throttle) {
        const [count, periodName] = throttle.split('/');
        const requests = Number(count);

        if (periodName === 'sec')
            return Math.floor(1000 / requests);
        else if (periodName === 'min')
            return Math.floor(60000 / requests);
        else if (periodName === 'hour')
            return Math.floor(3600000 / requests);
        else if (periodName === 'day')
            return Math.floor(86400000 / requests);
        else if (periodName === 'week')
            return Math.floor(604800000 / requests);

        return 0;
    }
}

module.exports = ThrottleMiddleware;
