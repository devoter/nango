module.exports = {
    BaseMiddleware: require('./base-middleware'),
    NangoHeadersMiddleware: require('./nango-headers-middleware'),
    TokenAuthMiddleware: require('./token-auth-middleware'),
    ThrottleMiddleware: require('./throttle-middleware'),
    AuthRequiredMiddleware: require('./auth-requred-middleware')
};
