const BaseMiddleware = require('./base-middleware');

class TokenAuthMiddleware extends BaseMiddleware {
    async processRequest(request, context) {
        const db = context.db;
        const token = request.orig.headers['authorization'];

        if (!db || !token) {
            TokenAuthMiddleware.setUnauthorized(request);
            return null;
        }

        const user = await db.authToken(token);
        if (!user) {
            TokenAuthMiddleware.setUnauthorized(request);
            return null;
        }

        request.isAuthorized = true;
        request.user = user;

        return null;
    }

    static setUnauthorized(request) {
        request.isAuthorized = false;
        request.user = null;
    }
}

module.exports = TokenAuthMiddleware;
