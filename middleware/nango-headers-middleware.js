const BaseMiddleware = require('./base-middleware');

class NangoHeadersMiddleware extends BaseMiddleware {
    processResponse(request, response, context) {
        const responseHeaders = response.headers || {};
        const settingsHeaders = context.settings.headers || {};
        response.headers = Object.assign({
            'Access-Control-Allow-Headers': 'Origin, Content-Type, X-Auth-Token',
            'Access-Control-Allow-Origin': context.settings.allowedHosts.join(',')
        }, settingsHeaders, responseHeaders);
    }
}

module.exports = NangoHeadersMiddleware;
