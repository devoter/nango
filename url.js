/**
 * Nango url generator
 *
 * @param {String|RegExp} path
 * @param {Function} view
 * @param {Array} methods
 * @returns {Array}
 */
function url(path, view, methods) {
    return [path, view, methods];
}

module.exports = url;
